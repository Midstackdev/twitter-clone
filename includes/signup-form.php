<?php  
	if(isset($_POST['signup'])){
		$screenName = $_POST['screenName'];
		$password   = $_POST['password'];
		$email 		= $_POST['email'];
		$error 		= '';

		if(empty($screenName) || empty($password) || empty($email)){
			$error = 'All fileds are required';

		}else{
			$email = $getFromU->checkInput($email);
			$screenName = $getFromU->checkInput($screenName);
			$password = $getFromU->checkInput($password);

			if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
				$error = 'Invalid email format';
			}else if(strlen($screenName) >20){
				$error = 'Name must be between in 6-20 characters';
			}else if(strlen($password) < 5){
				$error = 'Password must be at leat 6 characters';
			}else{
				if($getFromU->checkEmail($email) === true){
					$error = 'Email is already in use';
				}else{
					$ipath = "assets\images\defaultProfileImage.png";
					$cpath = "assets\images\defaultCoverImage.png";
					$user_id = $getFromU->create('users', array('email'=> $email, 'password' => md5($password), 
						'screenName' => $screenName, 'profileImage' => $ipath, 'profileCover' => $cpath ));
					$_SESSION['user_id'] = $user_id;
					header("Location: includes/signup.php?step=1");

				}
			}
		}

	}




?>




<form method="post">
<div class="signup-div"> 
	<h3>Sign up </h3>
	<ul>
		<li>
		    <input type="text" name="screenName" placeholder="Full Name" autocomplete="off"/>
		</li>
		<li>
		    <input type="email" name="email" placeholder="Email" autocomplete="off"/>
		</li>
		<li>
			<input type="password" name="password" placeholder="Password" autocomplete="off"/>
		</li>
		<li>
			<input type="submit" name="signup" Value="Signup for Twitter">
		</li>
	</ul>
	
	<?php 
		if(isset($error)){
			echo '
				<li class="error-li">
				  <div class="span-fp-error">'.$error.'</div>
				 </li>
			';
		}
	?>
	  
	
</div>
</form>