-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 18, 2019 at 09:04 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tweety`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `commentID` int(11) NOT NULL,
  `comment` varchar(140) NOT NULL,
  `commentOn` int(11) NOT NULL,
  `commentBy` int(11) NOT NULL,
  `commentAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`commentID`, `comment`, `commentOn`, `commentBy`, `commentAt`) VALUES
(1, 'test', 2, 2, '0000-00-00 00:00:00'),
(2, 'lets get kicking then', 4, 2, '2019-02-22 19:21:11'),
(3, 'lets get kicking then', 4, 2, '2019-02-22 19:21:11'),
(5, 'i am in', 7, 2, '2019-02-22 19:23:25'),
(7, 'hello food', 3, 2, '2019-02-25 16:00:53'),
(8, 'i am here', 3, 2, '2019-02-25 16:01:01'),
(9, 'yeah now is cool', 16, 7, '2019-02-26 11:42:13');

-- --------------------------------------------------------

--
-- Table structure for table `follow`
--

CREATE TABLE `follow` (
  `followID` int(11) NOT NULL,
  `sender` int(11) NOT NULL,
  `receiver` int(11) NOT NULL,
  `followOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `follow`
--

INSERT INTO `follow` (`followID`, `sender`, `receiver`, `followOn`) VALUES
(13, 2, 7, '0000-00-00 00:00:00'),
(14, 2, 1, '0000-00-00 00:00:00'),
(17, 7, 2, '2019-02-26 18:42:14'),
(18, 7, 1, '2019-02-26 18:42:32'),
(23, 1, 2, '2019-02-27 11:11:20'),
(24, 1, 7, '2019-02-27 11:11:22'),
(28, 6, 7, '2019-02-28 18:57:51'),
(29, 6, 2, '2019-02-28 18:58:23'),
(33, 2, 6, '2019-03-04 10:42:07');

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `likeID` int(11) NOT NULL,
  `likeBy` int(11) NOT NULL,
  `likeOn` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`likeID`, `likeBy`, `likeOn`) VALUES
(15, 2, 1),
(16, 2, 7),
(17, 2, 8),
(18, 7, 9),
(19, 7, 11),
(20, 7, 15);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `messageID` int(11) NOT NULL,
  `message` text NOT NULL,
  `messageTo` int(11) NOT NULL,
  `messageFrom` int(11) NOT NULL,
  `messageOn` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`messageID`, `message`, `messageTo`, `messageFrom`, `messageOn`, `status`) VALUES
(1, 'hi', 6, 7, '0000-00-00 00:00:00', 1),
(2, 'hi', 7, 6, '2019-02-28 11:37:58', 1),
(3, 'hi', 7, 1, '2019-05-27 13:37:47', 0);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `ID` int(11) NOT NULL,
  `notificationFor` int(11) NOT NULL,
  `notificationFrom` int(11) NOT NULL,
  `target` int(11) NOT NULL,
  `type` enum('follow','retweet','like','mention') NOT NULL,
  `time` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`ID`, `notificationFor`, `notificationFrom`, `target`, `type`, `time`, `status`) VALUES
(1, 6, 7, 30, 'mention', '2019-02-28 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `trends`
--

CREATE TABLE `trends` (
  `trendID` int(11) NOT NULL,
  `hashtag` varchar(140) NOT NULL,
  `createdOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trends`
--

INSERT INTO `trends` (`trendID`, `hashtag`, `createdOn`) VALUES
(1, 'php', '0000-00-00 00:00:00'),
(2, 'tag', '2019-02-20 17:05:41');

-- --------------------------------------------------------

--
-- Table structure for table `tweets`
--

CREATE TABLE `tweets` (
  `tweetID` int(11) NOT NULL,
  `status` varchar(140) NOT NULL,
  `tweetBy` int(11) NOT NULL,
  `retweetID` int(11) NOT NULL,
  `retweetBy` int(11) NOT NULL,
  `tweetImage` varchar(255) NOT NULL,
  `likesCount` int(11) NOT NULL,
  `retweetCount` int(11) NOT NULL,
  `postedOn` datetime NOT NULL,
  `retweetMsg` varchar(140) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tweets`
--

INSERT INTO `tweets` (`tweetID`, `status`, `tweetBy`, `retweetID`, `retweetBy`, `tweetImage`, `likesCount`, `retweetCount`, `postedOn`, `retweetMsg`) VALUES
(1, 'We are working in PHP', 2, 0, 0, '', 3, 0, '2019-02-14 16:09:00', ''),
(2, 'Something is working right here...', 2, 0, 0, '', 0, 1, '2019-02-14 17:34:41', ''),
(3, '', 2, 0, 0, 'users/homers.png', 0, 0, '2019-02-14 17:35:12', ''),
(4, 'My new tweet with #tag', 7, 0, 0, '', 0, 0, '2019-02-20 18:05:41', ''),
(5, 'My new tweet with #tag', 1, 0, 0, '', 0, 1, '2019-02-20 18:28:42', ''),
(6, 'my link here @dicey www.massdistros.com', 2, 0, 0, '', 0, 0, '2019-02-20 18:29:17', ''),
(7, 'http://www.massdistros.com is the link here', 2, 0, 0, '', 2, 0, '2019-02-20 18:30:08', ''),
(8, 'http://www.massdistros.com is the link here', 2, 0, 0, '', 2, 0, '2019-02-20 18:31:10', ''),
(9, 'http://www.massdistros.com is the link here', 2, 0, 0, '', 1, 1, '2019-02-20 18:31:21', ''),
(11, 'My new tweet with #tag', 1, 5, 2, '', 1, 1, '2019-02-21 18:49:15', 'test retweet'),
(14, '', 2, 0, 0, 'users/1_L5QyrMNalM3yhtgdgBcvkQ.png', 0, 0, '2019-02-25 19:01:20', ''),
(15, 'is this all i get now', 2, 0, 0, '', 1, 0, '2019-02-25 19:34:01', ''),
(16, 'How about now', 2, 0, 0, '', 0, 0, '2019-02-25 20:02:02', ''),
(17, 'How about now', 2, 0, 0, '', 0, 0, '2019-02-25 20:03:47', ''),
(18, 'How about now', 2, 0, 0, '', 0, 0, '2019-02-25 20:03:51', ''),
(19, 'Well it seems i have been missing out', 7, 0, 0, '', 0, 0, '2019-02-26 11:26:50', ''),
(20, 'THATS NOT ABOUT TO HAPPEN AGAIN', 7, 0, 0, '', 0, 0, '2019-02-26 11:27:22', ''),
(21, 'We are all set up to run the place now', 7, 0, 0, '', 0, 0, '2019-02-26 11:30:56', ''),
(22, 'Something is working right here...', 2, 2, 7, '', 0, 1, '2019-02-26 10:31:09', 'yeah right'),
(23, 'Main Chao', 7, 0, 0, 'users/samosa.jpg', 0, 0, '2019-02-26 12:08:02', ''),
(24, '#php is awesome to work with', 7, 0, 0, '', 0, 0, '2019-02-26 12:17:47', ''),
(25, 'http://www.massdistros.com is the link here', 2, 9, 7, '', 1, 1, '2019-02-20 18:31:21', 'check it out now'),
(26, 'I have been missing for a long long time man', 1, 0, 0, '', 0, 0, '2019-02-26 18:27:06', ''),
(27, 'I am back here and back for good', 1, 0, 0, '', 0, 0, '2019-02-26 18:27:20', ''),
(28, 'Let me updatde me details mate', 1, 0, 0, '', 0, 0, '2019-02-26 18:27:47', ''),
(29, 'Hello Polo', 1, 0, 0, 'users/polologo.png', 0, 0, '2019-02-26 18:28:06', ''),
(30, 'I am new here pls be nice', 6, 0, 0, '', 0, 0, '2019-02-27 13:47:40', ''),
(31, 'Wow i didn\'t know all these queries existed', 6, 0, 0, '', 0, 0, '2019-02-27 14:20:50', ''),
(32, 'Wow i didn\'t know all these queries existed', 6, 0, 0, '', 0, 0, '2019-02-27 14:20:50', ''),
(33, 'Why the double sending tho', 6, 0, 0, '', 0, 0, '2019-02-27 14:21:09', ''),
(34, '#php is actually very awesome', 6, 0, 0, '', 0, 0, '2019-02-28 18:11:01', ''),
(35, 'cool #php tricks', 6, 0, 0, 'users/1_L5QyrMNalM3yhtgdgBcvkQ.png', 0, 0, '2019-02-28 18:54:09', ''),
(36, '@lfrdsmit What\'s good bro', 6, 0, 0, '', 0, 0, '2019-03-01 19:50:37', ''),
(37, '@lfrdsmit you good', 6, 0, 0, '', 0, 0, '2019-03-01 20:03:03', ''),
(38, '@lfrdsmit you good', 6, 0, 0, '', 0, 0, '2019-03-01 20:13:09', ''),
(39, '@admin you', 6, 0, 0, '', 0, 0, '2019-03-01 20:24:06', ''),
(40, 'something is wrong here', 6, 0, 0, '', 0, 0, '2019-03-01 20:37:13', ''),
(41, 'hello there @admin', 6, 0, 0, '', 0, 0, '2019-03-02 08:43:39', ''),
(42, 'whats happening here @hansolo', 6, 0, 0, '', 0, 0, '2019-03-02 08:48:11', ''),
(43, 'whet is wrong with the application @dicey', 2, 0, 0, '', 0, 0, '2019-03-02 09:54:07', ''),
(44, '@admin you good?', 2, 0, 0, '', 0, 0, '2019-03-03 09:26:28', ''),
(45, '@admin whats up boy', 2, 0, 0, '', 0, 0, '2019-03-04 11:02:41', ''),
(46, '@lfrdsmit is my handle', 2, 0, 0, '', 0, 0, '2019-03-04 11:52:17', ''),
(47, '@admin it works', 2, 0, 0, '', 0, 0, '2019-03-04 11:53:48', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(40) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(32) NOT NULL,
  `screenName` varchar(40) NOT NULL,
  `profileImage` varchar(255) NOT NULL,
  `profileCover` varchar(255) NOT NULL,
  `following` int(11) NOT NULL,
  `followers` int(11) NOT NULL,
  `bio` varchar(140) NOT NULL,
  `country` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `email`, `password`, `screenName`, `profileImage`, `profileCover`, `following`, `followers`, `bio`, `country`, `website`) VALUES
(1, 'admin', 'admin@admin.com', '5f4dcc3b5aa765d61d8327deb882cf99', 'admino', 'users/Supplements.jpg', 'users/DRINKING-WINE.jpg', 3, 2, 'I love PHP Programiing', 'localhost', 'www.massdistros.com'),
(2, 'lfrdsmit', 'lfrdsmit@ymail.com', '5f4dcc3b5aa765d61d8327deb882cf99', 'alfred smith', 'users/banku.jpg', 'users/chocolate.jpg', 3, 3, 'i love php', '', ''),
(6, 'dicey', 'mickey@ymail.com', '5f4dcc3b5aa765d61d8327deb882cf99', 'Mickey Mouse', 'assets\\images\\defaultProfileImage.png', 'assets\\images\\defaultCoverImage.png', 2, 2, '', '', ''),
(7, 'hansolo', 'solo@gmail.com', '5f4dcc3b5aa765d61d8327deb882cf99', 'Han Solo', 'users/chopped-salad-1024x768.jpg', 'users/BeefStewside.jpg', 2, 3, '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`commentID`);

--
-- Indexes for table `follow`
--
ALTER TABLE `follow`
  ADD PRIMARY KEY (`followID`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`likeID`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`messageID`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `trends`
--
ALTER TABLE `trends`
  ADD PRIMARY KEY (`trendID`),
  ADD UNIQUE KEY `hashtag` (`hashtag`);

--
-- Indexes for table `tweets`
--
ALTER TABLE `tweets`
  ADD PRIMARY KEY (`tweetID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `commentID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `follow`
--
ALTER TABLE `follow`
  MODIFY `followID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `likeID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `messageID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `trends`
--
ALTER TABLE `trends`
  MODIFY `trendID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tweets`
--
ALTER TABLE `tweets`
  MODIFY `tweetID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
